import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const ExercicesScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Text>Exercices Screen</Text> 
            <Button 
                title = "Click here"
                onPress={() => alert('Button clicked !')}/>
        </View>
    ) ;
};

export default ExercicesScreen;

const styles = StyleSheet.create ({
    container: {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#8fcbbc'
    },
});