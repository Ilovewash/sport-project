import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';

const CommunityScreen = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Text>Community Screen</Text> 
            <Button 
                title = "Click here"
                onPress={() => alert('Button clicked !')}/>
        </View>
    ) ;
};

export default CommunityScreen;

const styles = StyleSheet.create ({
    container: {
        flex : 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#8fcbbc'
    },
});