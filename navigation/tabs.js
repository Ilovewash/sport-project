import React from 'react';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { View, Image, StyleSheet } from 'react-native';

import HomeScreen from "../screens/HomeScreen";
import ProfileScreen from "../screens/ProfileScreen";
import ExercicesScreen from "../screens/ExercicesScreen";
import CommunityScreen from "../screens/CommunityScreen";
import { Icon } from 'react-native-elements';

const Tab = createBottomTabNavigator();

export const data = [
    {
      id: '1',
      title: 'Community',
      image_url:
        require('../assets/Red_group_icon.png'),
      iconName: 'community',
    },
  
    {
      id: '2',
      title: 'Home',
      image_url:
        require('../assets/Red_home_icon.png'),
      iconName: 'home',
    },

    {
        id: '3',
        title: 'Exercices',
        image_url:
            require('../assets/Red_icon_dumbell.png'),
        iconName: 'exercices',
      },

      {
        id: '4',
        image_url:
            require('../assets/Red_profile_icon.png'),
        iconName: 'profile',
      },
    ];

    const screenOptions = (route, focused) => {
        let iconName;
       
        switch (route.name) {
            case 'Home':
                iconName = data[1].image_url;
                break;
            case 'Community':
                iconName = data[0].image_url;
                break;
            case 'Exercices':
                iconName = data[2].image_url;
                break;
            case 'Profile':
                iconName = data[3].image_url;
                break;
            default:
                break;
        }
      
        return <Image source={(iconName)} 
            style={{width: 30,
            height: 30,
            tintColor: focused ? 'red' : 'blue'}}/>;
      };
      

const Tabs = () => {
    return (
        <Tab.Navigator
            screenOptions = {({route, focused}) => ({
                showIcon: true ,
                
                tabBarStyle: {
                    position: 'absolute',
                    elevation: 0,
                    backgroundColor: '#fff',
                    borderTopLeftRadius : 15,
                    borderTopRightRadius : 15,
                    height: 60,
                },
                tabBarIcon:({focused}) => 
                        screenOptions(route),
                        showLabel : false,
                        tintColor : focused ? 'blue' : 'red',
                        
            })}
        >
            <Tab.Screen name="Community" component={CommunityScreen}/>
            <Tab.Screen name="Home" component={HomeScreen}/>
            <Tab.Screen name="Exercices" component={ExercicesScreen}/>
            <Tab.Screen name="Profile" component={ProfileScreen}/>
        </Tab.Navigator>
    );
};

export default Tabs;
