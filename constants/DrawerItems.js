export default [
    {
        //name: the name of the screen that the side menu navigates to
        name:'Profile',
        //iconType: the icon library where the icon is taken from
        iconType:'Material',
        //icon: the name of the icon to display next to each menu
        iconName:'face-profile'
    },
    {
        name:'Settings',
        iconType:'Feather',
        iconName:'settings'
    },
    {
        name:'Saved Items',
        iconType:'Material',
        iconName:'bookmark-check-outline'
    },
    {
        name:'Refer a Friend!',
        iconType:'FontAwesome5',
        iconName:'user-friends'
    }
  ]
  