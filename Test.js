import React,  { useState } from 'react';
import {FlatList, StyleSheet, Button, ScrollView, Text, TextInput, View, Image } from 'react-native';

const getFullname = (firstName, secondName) => {
  return firstName + " fall in love with " + secondName;
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

const Cat = () => {
  const name = "Colin";
  
    return (
    <View
      style={{
      justifyContent: 'center', //Centered horizontally
      alignItems: 'center', //Centered vertically
      flex:1
    }}>
      <Text >Hello, I am your cat, {getFullname("colin", "matteo")}!</Text>
      <TextInput style={{
          height: 40,
          borderColor: 'gray',
          borderWidth: 1,
          width: 200,
          padding: 10
        }}>

        </TextInput>
    </View>
    );
};

const FlatListBasics = () => {
  return (
    <View style={styles.container}>
      <FlatList
        data={[
          {key: 'Devin'},
          {key: 'Dan'},
          {key: 'Dominic'},
          {key: 'Jackson'},
          {key: 'James'},
          {key: 'Joel'},
          {key: 'John'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]}
        renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
      />
    </View>
  );
}

const Cat2 = (props) => {
  const [isHungry, setIsHungry] = useState(true);
    return (
      <View>
        <Text>Hi i'm {props.name}, and i am {isHungry ? "hungry" : "full"}!{"\n"}</Text>
        <Button 
          onPress={() => {
            setIsHungry(false);
          }}
          disabled={!isHungry}
          title={isHungry ? "Pour me some milk, please!" : "Thank you!"}
          />
        </View>
    );
};

const Family = () => {
  return (
    <View style={{flex: 1}}>
      <View 
      style={{
        borderWidth: 4, 
        borderColor: "#000", 
      flex: 4,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'}}>
        < >
        
          <Text>This is my family{"\n"}</Text>
          <Cat2 name="Colin"/>
          <Cat2 name="Mattéo"/>
        </>
        </View>
      <View style={{flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'}}>
            <FlatListBasics/>
          </View>
      </View>
  )
}
export default Family;
